<?php

/**
 * @file
 * Installation routines for this module.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

use Drupal\Core\Url;

/**
 * Implements hook_install().
 *
 * Adds or removes the 'cached_moderation_state' field where necessary.
 */
function cached_moderation_state_install() {
  // Increase the weight of this module so that its hooks are invoked last.
  \module_set_weight('cached_moderation_state', 100);

  /** @var \Drupal\cached_moderation_state\FieldConfigHandlerInterface */
  $field_config_handler = \Drupal::service('cached_moderation_state.field_config_handler');
  $field_config_handler->sync();
}

/**
 * Implements hook_requirements().
 *
 * Adds a row to the status report detailing whether there are moderated
 * entities with uninitialized 'cached_moderation_state' fields.
 */
function cached_moderation_state_requirements($phase) {
  $requirements = [];

  if ($phase === 'runtime') {
    $requirements['cached_moderation_state'] = [
      'severity' => \REQUIREMENT_OK,
      'title' => \t('Cached moderation state'),
      'value' => \t('Initialized for all moderated entities'),
    ];

    /** @var \Drupal\cached_moderation_state\BatchUpdateHandlerInterface */
    $batch_update_handler = \Drupal::service('cached_moderation_state.batch_update_handler');
    /** @var \Drupal\cached_moderation_state\FieldConfigHandlerInterface */
    $field_config_handler = \Drupal::service('cached_moderation_state.field_config_handler');

    $uninitialized_count = 0;
    foreach ($field_config_handler->getModeratedEntityTypes() as $entity_type) {
      $bundles = $field_config_handler->getModeratedBundlesForEntityType($entity_type);
      $entity_ids = $batch_update_handler->getEntitiesForEntityType($entity_type->id(), $bundles, TRUE);

      $uninitialized_count += \count($entity_ids);
    }

    if ($uninitialized_count > 0) {
      $description = \Drupal::translation()->formatPlural(
        $uninitialized_count,
        '1 moderated entity has an uninitialized <code>cached_moderation_state</code> field. Use the <a href=":url">update form</a> to initialize it.',
        '@count moderated entities have an uninitialized <code>cached_moderation_state</code> field. Use the <a href=":url">update form</a> to initialize them.',
        [
          ':url' => Url::fromRoute('cached_moderation_state.batch_update_form')->toString(),
        ],
      );

      $requirements['cached_moderation_state'] = [
        'severity' => \REQUIREMENT_WARNING,
        'title' => \t('Cached moderation state'),
        'value' => \t('Not fully initialized'),
        'description' => $description,
      ];
    }
  }

  return $requirements;
}
