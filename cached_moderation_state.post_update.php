<?php

/**
 * @file
 * Post-update routines for this module.
 *
 * Copyright (C) 2024  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/**
 * Inject the logger factory to the batch update handler.
 */
function cached_moderation_state_post_update_inject_logger_factory_to_batch_update_handler() {
  // This update hook is intentionally empty.
}
