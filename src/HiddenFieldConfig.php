<?php

namespace Drupal\cached_moderation_state;

use Drupal\field\Entity\FieldConfig;

/**
 * Provides a drop-in field config replacement that is hidden in the Field UI.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @see \Drupal\field\Entity\FieldConfig
 *
 * @internal
 */
class HiddenFieldConfig extends FieldConfig {

  /**
   * {@inheritdoc}
   */
  public function isDisplayConfigurable(...$args): bool {
    return FALSE;
  }

}
