<?php

namespace Drupal\cached_moderation_state;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handles the creation and deletion of field instances for moderated entities.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class FieldConfigHandler implements ContainerInjectionInterface, FieldConfigHandlerInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The entity type bundle information service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The field config storage class.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fieldConfigStorage;

  /**
   * The field storage config storage class.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fieldStorageConfigStorage;

  /**
   * The moderation information service.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface
   */
  protected $moderationInformation;

  /**
   * Constructs a FieldConfigHandler object.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle information service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\content_moderation\ModerationInformationInterface $moderation_information
   *   The moderation information service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   If the field config or field storage config storage class does not exist.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   If the field config or field storage config storage class does not exist.
   */
  public function __construct(EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityTypeManagerInterface $entity_type_manager, ModerationInformationInterface $moderation_information) {
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldConfigStorage = $entity_type_manager->getStorage('field_config');
    $this->fieldStorageConfigStorage = $entity_type_manager->getStorage('field_storage_config');
    $this->moderationInformation = $moderation_information;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.bundle.info'),
      $container->get('entity_type.manager'),
      $container->get('content_moderation.moderation_information'),
    );
  }

  /**
   * Deletes the 'cached_moderation_state' field for unmoderated bundles.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   If a field config entity could not be deleted.
   */
  protected function deleteFields(): void {
    foreach ($this->getCachedModerationStateFields() as $cached_moderation_state_field) {
      $bundle = $cached_moderation_state_field->getTargetBundle();
      $entity_type_id = $cached_moderation_state_field->getTargetEntityTypeId();

      if (!$this->shouldModerateEntitiesOfBundle($entity_type_id, $bundle)) {
        $cached_moderation_state_field->delete();
      }
    }
  }

  /**
   * Installs the 'cached_moderation_state' field for the specified bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID for which to install the field.
   * @param string $bundle
   *   The bundle for which to install the field.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   If the field config entity could not be saved.
   */
  protected function ensureField(string $entity_type_id, string $bundle): void {
    $config = [
      'bundle' => $bundle,
      'entity_type' => $entity_type_id,
      'field_name' => 'cached_moderation_state',
      'label' => $this->t('Cached moderation state'),
    ];

    if (!$this->fieldConfigStorage->load("{$entity_type_id}.{$bundle}.cached_moderation_state")) {
      $this->fieldConfigStorage->create($config)->save();
    }
  }

  /**
   * Installs the 'cached_moderation_state' field for the specified entity type.
   *
   * @param string $entity_type_id
   *   The entity type ID for which to install the field.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   If the field storage config entity could not be saved.
   */
  protected function ensureFieldStorage(string $entity_type_id): void {
    $config = [
      'entity_type' => $entity_type_id,
      'field_name' => 'cached_moderation_state',
      'locked' => TRUE,
      'type' => 'cached_moderation_state',
    ];

    if (!$this->fieldStorageConfigStorage->load("{$entity_type_id}.cached_moderation_state")) {
      $this->fieldStorageConfigStorage->create($config)->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCachedModerationStateFields() {
    $properties = [
      'field_type' => 'cached_moderation_state',
    ];

    $fields = $this->fieldConfigStorage->loadByProperties($properties);
    $fields = \array_values($fields);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getModeratedBundlesForEntityType(EntityTypeInterface|string $entity_type) {
    if (\is_string($entity_type) && !$entity_type = $this->entityTypeManager->getDefinition($entity_type, FALSE)) {
      return [];
    }

    $filter = $this->shouldModerateEntitiesOfBundle(...);

    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type->id());
    $bundles = \array_fill_keys(\array_keys($bundles), $entity_type->id());
    $bundles = \array_filter($bundles, $filter, \ARRAY_FILTER_USE_BOTH);
    $bundles = \array_keys($bundles);

    return $bundles;
  }

  /**
   * {@inheritdoc}
   */
  public function getModeratedEntityTypes() {
    $filter = $this->getModeratedBundlesForEntityType(...);

    $entity_types = $this->entityTypeManager->getDefinitions();
    $entity_types = \array_values(\array_filter($entity_types, $filter));

    return $entity_types;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldModerateEntitiesOfBundle(EntityTypeInterface|string $entity_type, string $bundle): bool {
    if (\is_string($entity_type) && !$entity_type = $this->entityTypeManager->getDefinition($entity_type, FALSE)) {
      return FALSE;
    }

    return $this->moderationInformation->shouldModerateEntitiesOfBundle($entity_type, $bundle);
  }

  /**
   * {@inheritdoc}
   */
  public function sync(): static {
    $this->deleteFields();

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type) {
      $bundles = $this->getModeratedBundlesForEntityType($entity_type);

      foreach ($bundles as $bundle) {
        $this->ensureFieldStorage($entity_type->id());
        $this->ensureField($entity_type->id(), $bundle);
      }
    }

    return $this;
  }

}
