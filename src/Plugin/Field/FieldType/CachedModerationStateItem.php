<?php

namespace Drupal\cached_moderation_state\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'cached_moderation_state' field type.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @FieldType(
 *   id = "cached_moderation_state",
 *   label = @Translation("Cached moderation state"),
 *   description = @Translation("Caches the moderation state of an entity."),
 *   cardinality = 1,
 *   no_ui = TRUE,
 * )
 */
class CachedModerationStateItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function __get($name) {
    $this->updateItemValue();
    return parent::__get($name);
  }

  /**
   * {@inheritdoc}
   */
  public function __isset($name) {
    $this->updateItemValue();
    return parent::__isset($name);
  }

  /**
   * {@inheritdoc}
   */
  public function get($property_name) {
    $this->updateItemValue();
    return parent::get($property_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getProperties($include_computed = FALSE) {
    $this->updateItemValue();
    return parent::getProperties($include_computed);
  }

  /**
   * {@inheritdoc}
   */
  public function getString() {
    $this->updateItemValue();
    return parent::getString();
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    $this->updateItemValue();
    return parent::getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $this->updateItemValue();
    return parent::isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    $this->updateItemValue();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string');
    $properties['value']->setLabel(\t('Cached moderation state'));

    $properties['updated'] = DataDefinition::create('timestamp');
    $properties['updated']->setLabel(\t('A timestamp representing when the field value was last updated'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'binary' => TRUE,
          'length' => 191,
          'type' => 'varchar_ascii',
        ],
        'updated' => [
          'size' => 'big',
          'type' => 'int',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function set($property_name, $value, $notify = TRUE) {
    return $this->updateItemValue($notify);
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    $this->updateItemValue($notify);
  }

  /**
   * Update the item value to reflect the entity's current moderation state.
   *
   * @param bool $notify
   *   Whether to notify the parent object of the change (default: TRUE).
   *
   * @return $this
   *   The current object for method chaining.
   */
  protected function updateItemValue(bool $notify = TRUE) {
    $this->writePropertyValue('value', $this->getEntity()->moderation_state?->value);
    $this->onChange('value', $notify);

    $this->writePropertyValue('updated', \Drupal::time()->getCurrentTime());
    $this->onChange('updated', $notify);

    return $this;
  }

}
