<?php

namespace Drupal\cached_moderation_state;

use Drupal\Core\Entity\EntityInterface;

/**
 * An interface used to facilitate batch updates to cached_moderation_state.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
interface BatchUpdateHandlerInterface {

  /**
   * Get a list of entity identifiers, filtered by bundle.
   *
   * Each identifier will be formatted as `{$entity_type_id}:{$id}`.
   *
   * If the supplied entity type is revisionable, all revisions will be queried
   * and the resulting list of identifiers should be interpreted as entity
   * revision identifiers.
   *
   * @param string $entity_type_id
   *   The identifier of the entity type for which to enumerate its entities.
   * @param string[] $bundles
   *   A list of bundles used to filter the results. If no bundles are
   *   specified, bundle filtering will not occur (default: []).
   * @param bool $only_uninitialized
   *   TRUE to only query entities with uninitialized fields (default: FALSE).
   *
   * @return string[]
   *   A list of entity identifiers.
   */
  public function getEntitiesForEntityType(string $entity_type_id, array $bundles = [], bool $only_uninitialized = FALSE);

  /**
   * Check if an entity update caused by this service is in progress.
   *
   * This method is used to facilitate disabling the creation of new revisions
   * while this service is performing entity updates.
   *
   * @see cached_moderation_state_entity_presave()
   *   For more information about how revision creation is disabled.
   *
   * @return bool
   *   TRUE if an entity update caused by this service is in progress,
   *   FALSE otherwise.
   */
  public function isEntityUpdateInProgress(): bool;

  /**
   * Load a list of entities identified by `{$entity_type_id}:{$id}`.
   *
   * For each revisionable entity type, `{$id}` is treated as a revision
   * identifier for the purposes of loading the entity.
   *
   * If this method is supplied with a set of entities that span more than one
   * entity type, multiple successive database queries may be incurred.
   *
   * @param string[] $entity_ids
   *   A list of entity identifiers (`{$entity_type_id}:{$id}`).
   *
   * @see ::getEntitiesForEntityType()
   *   To get entity identifiers in the format this method requires.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   A list of entities.
   */
  public function loadEntities(array $entity_ids);

  /**
   * Loads a batch of entities and updates the 'cached_moderation_state' field.
   *
   * This method can be used for batching content updates. Exceptions will be
   * logged in a way that allows successive entities to be processed.
   *
   * @param string[] $entity_ids
   *   A list of entity identifiers (`{$entity_type_id}:{$id}`).
   * @param int $batch_size
   *   The number of entities to load and update. Must be an integer value
   *   greater than or equal to 0. If this parameter is 0, batching will not be
   *   used for entity updates (default: 0).
   *
   * @throws \InvalidArgumentException
   *   If $batch_size is less than 0.
   *
   * @see ::getEntitiesForEntityType()
   *   To get entity identifiers in the format this method requires.
   *
   * @return int
   *   The number of entities that were removed from the input.
   */
  public function updateEntities(array &$entity_ids, int $batch_size = 0): int;

  /**
   * Updates the 'cached_moderation_state' field for an entity.
   *
   * If an exception is thrown while updating the entity, it will be logged
   * safely so successive updates can occur without incident.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to update.
   */
  public function updateEntity(EntityInterface $entity): void;

}
