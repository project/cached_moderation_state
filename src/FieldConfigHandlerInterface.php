<?php

namespace Drupal\cached_moderation_state;

use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Provides an interface for managing 'cached_moderation_state' fields.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
interface FieldConfigHandlerInterface {

  /**
   * Get a list of 'cached_moderation_state' field instances.
   *
   * @return \Drupal\field\FieldConfigInterface[]
   *   A list of 'cached_moderation_state' field instances.
   */
  public function getCachedModerationStateFields();

  /**
   * Get a list of moderated bundle identifiers for the supplied entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface|string $entity_type
   *   The entity type for which to retrieve its moderated bundles.
   *
   * @return string[]
   *   A list of moderated bundle identifiers.
   */
  public function getModeratedBundlesForEntityType(EntityTypeInterface|string $entity_type);

  /**
   * Get a list of moderated entity types.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   *   A list of moderated entity types.
   */
  public function getModeratedEntityTypes();

  /**
   * Check if the supplied entity type and bundle combination is moderated.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface|string $entity_type
   *   The entity type to check.
   * @param string $bundle
   *   The bundle to check.
   *
   * @return bool
   *   TRUE if moderated, FALSE otherwise.
   */
  public function shouldModerateEntitiesOfBundle(EntityTypeInterface|string $entity_type, string $bundle): bool;

  /**
   * Synchronize field instances with moderated entity bundles.
   *
   * This method will create or delete 'cached_moderation_state' field instances
   * as needed to establish a one-to-one relationship of field instances to
   * moderated entity bundles.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   If an error occurred while handling a field storage config or field
   *   config entity during synchronization.
   *
   * @return static
   *   $this for method chaining.
   */
  public function sync(): static;

}
