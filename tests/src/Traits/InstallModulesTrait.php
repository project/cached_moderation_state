<?php

namespace Drupal\Tests\cached_moderation_state\Traits;

/**
 * Provides a trait to make installing modules easier.
 *
 * Copyright (C) 2025  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
trait InstallModulesTrait {

  /**
   * Installs a given list of modules.
   *
   * @param string[] $module_list
   *   An array of module names.
   */
  protected function installModules(array $module_list): void {
    /** @var \Drupal\Core\Extension\ModuleInstallerInterface */
    $module_installer = \Drupal::service('module_installer');
    $module_installer->install($module_list);

    // Use the rebuilt container after modules were installed.
    $this->container = \Drupal::getContainer();
  }

}
