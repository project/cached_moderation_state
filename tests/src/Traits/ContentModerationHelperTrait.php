<?php

namespace Drupal\Tests\cached_moderation_state\Traits;

use Drupal\node\NodeTypeInterface;
use Drupal\workflows\WorkflowInterface;

/**
 * Provides a trait to make creating moderated content types easier.
 *
 * Copyright (C) 2025  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
trait ContentModerationHelperTrait {

  /**
   * Add the specified entity type and bundle to the supplied workflow.
   *
   * @param \Drupal\workflows\WorkflowInterface $workflow
   *   The workflow to modify.
   * @param string $entity_type_id
   *   The entity type ID to add.
   * @param string $bundle_id
   *   The bundle to add.
   */
  protected function addEntityTypeAndBundleToWorkflow(WorkflowInterface $workflow, string $entity_type_id, string $bundle_id): void {
    /** @var \Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface */
    $workflow_type = $workflow->getTypePlugin();
    $workflow_type->addEntityTypeAndBundle($entity_type_id, $bundle_id);

    $workflow->save();
  }

  /**
   * Create a content type.
   *
   * @param array $values
   *   An array of settings to change from the defaults.
   *
   * @return \Drupal\node\NodeTypeInterface
   *   The content type.
   */
  protected function createContentType(array $values = []): NodeTypeInterface {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
    $entity_type_manager = \Drupal::service('entity_type.manager');

    $node_type_storage = $entity_type_manager->getStorage('node_type');
    $node_type_id = $this->randomMachineName();

    $defaults = [
      'name' => $node_type_id,
      'type' => $node_type_id,
    ];

    $node_type = $node_type_storage->create($values + $defaults);
    $node_type->save();

    return $node_type;
  }

  /**
   * Create a content type moderated by a content moderation workflow.
   *
   * @param array $values
   *   An array of settings to change from the defaults.
   *
   * @see ::createContentType()
   *   For more information about how content types are created.
   *
   * @return \Drupal\node\NodeTypeInterface
   *   The content type.
   */
  protected function createModeratedContentType(array $values = []): NodeTypeInterface {
    $node_type = $this->createContentType($values);

    $this->createModerationWorkflow([
      $node_type->id(),
    ]);

    return $node_type;
  }

  /**
   * Create a moderation workflow with an optional set of initial node bundles.
   *
   * @param array $bundles
   *   A set of node bundles to moderate when creating the workflow.
   *
   * @return \Drupal\workflows\WorkflowInterface
   *   The moderation workflow.
   */
  protected function createModerationWorkflow(array $bundles = []): WorkflowInterface {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
    $entity_type_manager = \Drupal::service('entity_type.manager');

    $workflow_storage = $entity_type_manager->getStorage('workflow');
    $workflow_id = $this->randomMachineName();

    /** @var \Drupal\workflows\WorkflowInterface */
    $workflow = $workflow_storage->create([
      'id' => $workflow_id,
      'label' => $workflow_id,
      'type' => 'content_moderation',
    ]);

    /** @var \Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface */
    $workflow_type = $workflow->getTypePlugin();

    foreach ($bundles as $bundle) {
      $workflow_type->addEntityTypeAndBundle('node', $bundle);
    }

    $workflow->save();

    return $workflow;
  }

  /**
   * Remove the specified entity type and bundle from the supplied workflow.
   *
   * @param \Drupal\workflows\WorkflowInterface $workflow
   *   The workflow to modify.
   * @param string $entity_type_id
   *   The entity type ID to remove.
   * @param string $bundle_id
   *   The bundle to remove.
   */
  protected function removeEntityTypeAndBundleFromWorkflow(WorkflowInterface $workflow, string $entity_type_id, string $bundle_id): void {
    /** @var \Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface */
    $workflow_type = $workflow->getTypePlugin();
    $workflow_type->removeEntityTypeAndBundle($entity_type_id, $bundle_id);

    $workflow->save();
  }

}
