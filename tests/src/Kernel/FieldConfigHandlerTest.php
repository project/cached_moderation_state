<?php

namespace Drupal\Tests\cached_moderation_state\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\Tests\cached_moderation_state\Traits\ContentModerationHelperTrait;
use Drupal\Tests\cached_moderation_state\Traits\InstallModulesTrait;

/**
 * Tests the field config handler service and related hooks.
 *
 * Copyright (C) 2025  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @group cached_moderation_state
 */
class FieldConfigHandlerTest extends KernelTestBase {

  use ContentModerationHelperTrait;
  use InstallModulesTrait;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installModules([
      'content_moderation',
      'node',
    ]);
  }

  /**
   * Test moderated entities have a 'cached_moderation_state' item on creation.
   */
  public function testFieldItemCreationOnEntityCreate() {
    $this->installModules(['cached_moderation_state']);

    $node = Node::create([
      'title' => $this->randomMachineName(),
      'type' => $this->createModeratedContentType()->id(),
    ]);

    $this->assertTrue($node->hasField('cached_moderation_state'));
    $this->assertNotEmpty($node->cached_moderation_state->first());
  }

  /**
   * Test moderated entities have a 'cached_moderation_state' item on load.
   */
  public function testFieldItemCreationOnEntityLoad() {
    $node = Node::create([
      'title' => $this->randomMachineName(),
      'type' => $this->createModeratedContentType()->id(),
    ]);

    $node->save();

    // NOTE: The module must be installed after moderated content is created.
    $this->installModules(['cached_moderation_state']);
    $this->assertFalse($node->hasField('cached_moderation_state'));

    $node = Node::load($node->id());

    $this->assertTrue($node->hasField('cached_moderation_state'));
    $this->assertNotEmpty($node->cached_moderation_state->first());
  }

  /**
   * Test 'cached_moderation_state' is added to moderated bundles on install.
   */
  public function testFieldSyncOnInstall() {
    $moderated = "node.{$this->createModeratedContentType()->id()}.cached_moderation_state";
    $unmoderated = "node.{$this->createContentType()->id()}.cached_moderation_state";

    $this->assertEmpty(FieldConfig::load($moderated));
    $this->assertEmpty(FieldConfig::load($unmoderated));

    // NOTE: The module must be installed after the content types are created.
    $this->installModules(['cached_moderation_state']);

    $this->assertNotEmpty(FieldConfig::load($moderated));
    $this->assertEmpty(FieldConfig::load($unmoderated));
  }

  /**
   * Test 'cached_moderation_state' is added when inserting a workflow.
   */
  public function testFieldSyncOnWorkflowInsert() {
    $this->installModules(['cached_moderation_state']);

    $bundle = $this->createContentType()->id();
    $config = "node.{$bundle}.cached_moderation_state";

    $this->assertEmpty(FieldConfig::load($config));
    $this->createModerationWorkflow([$bundle]);
    $this->assertNotEmpty(FieldConfig::load($config));
  }

  /**
   * Test 'cached_moderation_state' is added when updating a workflow.
   */
  public function testFieldSyncOnWorkflowUpdate() {
    $this->installModules(['cached_moderation_state']);

    $bundle = $this->createContentType()->id();
    $config = "node.{$bundle}.cached_moderation_state";

    $workflow = $this->createModerationWorkflow();

    $this->assertEmpty(FieldConfig::load($config));
    $this->addEntityTypeAndBundleToWorkflow($workflow, 'node', $bundle);
    $this->assertNotEmpty(FieldConfig::load($config));
    $this->removeEntityTypeAndBundleFromWorkflow($workflow, 'node', $bundle);
    $this->assertEmpty(FieldConfig::load($config));
  }

}
