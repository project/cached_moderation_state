<?php

namespace Drupal\Tests\cached_moderation_state\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\Tests\cached_moderation_state\Traits\ContentModerationHelperTrait;
use Drupal\Tests\cached_moderation_state\Traits\InstallModulesTrait;

/**
 * Tests the 'cached_moderation_state' field type.
 *
 * Copyright (C) 2025  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @group cached_moderation_state
 */
class CachedModerationStateItemTest extends KernelTestBase {

  use ContentModerationHelperTrait;
  use InstallModulesTrait;

  /**
   * A moderated node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installModules([
      'cached_moderation_state',
      'node',
    ]);

    $this->node = Node::create([
      'title' => $this->randomMachineName(),
      'type' => $this->createModeratedContentType()->id(),
    ]);
  }

  /**
   * Test reading and writing 'content_moderation_state'.
   */
  public function testCachedModerationStateReadWrite() {
    $this->assertTrue($this->node->hasField('cached_moderation_state'));
    $this->assertNotEmpty($this->node->moderation_state->value);

    /** @var \Drupal\cached_moderation_state\Plugin\Field\FieldType\CachedModerationStateItem */
    $field = $this->node->cached_moderation_state->first();

    $property = $field->get('value');
    $property->setValue(NULL);

    // Tests that reading 'cached_moderation_state' copies 'moderation_state'.
    $this->assertEmpty($property->getValue());
    $this->assertSame($this->node->moderation_state->value, $this->node->cached_moderation_state->value);
    $this->assertSame($this->node->moderation_state->value, $property->getValue());

    $property->setValue(NULL);

    // Tests that writing 'cached_moderation_state' copies 'moderation_state'.
    $this->assertEmpty($property->getValue());
    $this->node->cached_moderation_state->value = NULL;
    $this->assertNotEmpty($this->node->moderation_state->value);
    $this->assertSame($this->node->moderation_state->value, $property->getValue());
  }

  /**
   * Test saving 'content_moderation_state'.
   */
  public function testCachedModerationStateSave() {
    /** @var \Drupal\cached_moderation_state\Plugin\Field\FieldType\CachedModerationStateItem */
    $field = $this->node->cached_moderation_state->first();

    $property = $field->get('value');
    $property->setValue(NULL);

    $this->assertEmpty($property->getValue());
    $this->node->save();
    $this->assertSame($this->node->moderation_state->value, $property->getValue());
  }

}
