<?php

namespace Drupal\Tests\cached_moderation_state\Functional;

use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\cached_moderation_state\Traits\ContentModerationHelperTrait;
use Drupal\Tests\cached_moderation_state\Traits\InstallModulesTrait;

/**
 * Tests the batch update form.
 *
 * Copyright (C) 2025  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @group cached_moderation_state
 */
class BatchUpdateTest extends BrowserTestBase {

  use ContentModerationHelperTrait;
  use InstallModulesTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'node',
  ];

  /**
   * Count the number of uninitialized entities.
   *
   * @return int
   *   The number of uninitialized entities.
   */
  protected function countUninitializedEntities(): int {
    $uninitialized_count = 0;

    /** @var \Drupal\cached_moderation_state\BatchUpdateHandlerInterface */
    $batch_update_handler = \Drupal::service('cached_moderation_state.batch_update_handler');
    /** @var \Drupal\cached_moderation_state\FieldConfigHandlerInterface */
    $field_config_handler = \Drupal::service('cached_moderation_state.field_config_handler');

    foreach ($field_config_handler->getModeratedEntityTypes() as $entity_type) {
      $bundles = $field_config_handler->getModeratedBundlesForEntityType($entity_type);
      $entity_ids = $batch_update_handler->getEntitiesForEntityType($entity_type->id(), $bundles, TRUE);

      $uninitialized_count += \count($entity_ids);
    }

    return $uninitialized_count;
  }

  /**
   * Data provider for ::testBatchUpdate().
   */
  public function providerTestBatchUpdate(): array {
    return [
      'Process 3 nodes with a batch size of 5' => [
        3,
        5,
        FALSE,
        'Updated 3 of 3 entities.',
        'Updated 3 of 3 entities.',
      ],
      'Process 3 uninitialized nodes with a batch size of 5' => [
        3,
        5,
        TRUE,
        'Updated 3 of 3 entities.',
        'No entities required updates.',
      ],
      'Process 15 nodes with a batch size of 5' => [
        15,
        5,
        FALSE,
        'Updated 15 of 15 entities.',
        'Updated 15 of 15 entities.',
      ],
      'Process 15 uninitialized nodes with a batch size of 5' => [
        15,
        5,
        TRUE,
        'Updated 15 of 15 entities.',
        'No entities required updates.',
      ],
    ];
  }

  /**
   * Test the batch update form.
   *
   * @dataProvider providerTestBatchUpdate
   */
  public function testBatchUpdate(int $node_count, int $batch_size, bool $only_uninitialized, string $first_run_message, string $second_run_message) {
    $node_type = $this->createModeratedContentType();
    $bundle = $node_type->id();

    for ($i = 0; $i < $node_count; ++$i) {
      $node = Node::create([
        'title' => $this->randomMachineName(),
        'type' => $bundle,
      ]);

      $node->save();

      $this->assertNotEmpty($node->moderation_state->value);
      $this->assertFalse($node->hasField('cached_moderation_state'));
    }

    $this->installModules(['cached_moderation_state']);
    $this->assertSame($node_count, $this->countUninitializedEntities());

    $this->drupalLogin($this->drupalCreateUser([
      'access cached_moderation_state update_form',
    ]));

    $this->drupalGet(Url::fromRoute('cached_moderation_state.batch_update_form'));

    $form_values = [
      'batch_size' => $batch_size,
      'bundles' => ["node:{$bundle}"],
      'only_uninitialized' => $only_uninitialized,
    ];

    $this->submitForm($form_values, 'Update cached moderation states');

    $this->assertSame(0, $this->countUninitializedEntities());
    $this->assertSession()->statusMessageContains($first_run_message, 'status');

    $this->submitForm($form_values, 'Update cached moderation states');

    $this->assertSame(0, $this->countUninitializedEntities());
    $this->assertSession()->statusMessageContains($second_run_message, 'status');
  }

}
