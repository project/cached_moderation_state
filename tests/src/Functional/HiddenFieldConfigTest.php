<?php

namespace Drupal\Tests\cached_moderation_state\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\cached_moderation_state\Traits\ContentModerationHelperTrait;

/**
 * Tests the hidden field config class.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @group cached_moderation_state
 */
class HiddenFieldConfigTest extends BrowserTestBase {

  use ContentModerationHelperTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'cached_moderation_state',
    'content_moderation',
    'field_ui',
    'node',
  ];

  /**
   * Test that 'Cached moderation state' doesn't appear in the Field UI.
   */
  public function testHiddenFieldConfig() {
    $bundle = $this->createModeratedContentType()->id();

    $this->drupalLogin($this->drupalCreateUser([
      'administer node fields',
      'administer node form display',
    ]));

    $this->drupalGet(Url::fromRoute('entity.node.field_ui_fields', [
      'node_type' => $bundle,
    ]));

    $this->assertSession()->pageTextContains('Manage fields');
    $this->assertSession()->pageTextContains('Cached moderation state');

    $this->drupalGet(Url::fromRoute('entity.entity_form_display.node.default', [
      'node_type' => $bundle,
    ]));

    $this->assertSession()->pageTextContains('Manage form display');
    $this->assertSession()->pageTextNotContains('Cached moderation state');
  }

}
